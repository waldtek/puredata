# puredata

This repo contains a short puredata class with some examples and a webserver.
Use pandoc to generate the index.html in the source directory.
The included webserver can be used to host the course on the LAN.

Dependencies:

* pandoc
* python3 bottle framework

# todo

* have each chapter be more verbose
* restructure so that there is a intro section with video examples of nice patches
* more robust end patches
* french translation

