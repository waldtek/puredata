<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="./markdown.css" rel="stylesheet"></link>
<style>
img:hover{
    color: #424242; 
  -webkit-transition: all .3s ease-in;
  -moz-transition: all .3s ease-in;
  -ms-transition: all .3s ease-in;
  -o-transition: all .3s ease-in;
  transition: all .3s ease-in;
  opacity: 1;
  transform: scale(2);
  -ms-transform: scale(2); /* IE 9 */
  -webkit-transform: scale(2); /* Safari and Chrome */
}
</style>


# Pure Data

![intro](./resources/pd_intro.jpg)

## Introduction

### What is Pure data?

Pure Data (or Pd) is a real-time graphical programming environment for audio, video, and graphical processing.
Pure Data is commonly used for live music performance, VeeJaying, sound effects, composition, audio analysis, interfacing with sensors, using cameras, controlling robots or even interacting with websites.
Because all of these various media are handled as digital data within the program, many fascinating opportunities for cross-synthesis between them exist.
Sound can be used to manipulate video, which could then be streamed over the internet to another computer which might analyze that video and use it to control a motor-driven installation.

*taken from [here](http://write.flossmanuals.net/pure-data/introduction2/)*

### How to get it?

You can download puredata from the [main website](http://puredata.info/downloads/pure-data).

#### Different versions

There are a few different versions of puredata available in the wild.
Below is a small list but I would *highly* advise to start with puredata vanilla.
The extended version is *very* dated now and I would not advise to use that one.

* puredata vanilla (the **main** version)
* puredata extended (up until a few years ago the go-to version for most people)
* Pd-L2Ork (same engine as vanilla but with different GUI)

#### A note on libpd

Puredata can be packaged into other applications as a sound engine.
This is extensively used by app makers and game designers.
While this is totally beyond the scope of this introduction I do want to mention the universal usefulness of puredata outside of sound creation.
Below are some useful links:

* [libpd github](https://github.com/libpd)
* [libpd in unity3D](https://github.com/Magicolo/uPD)
* [camomille - VST creating with libpd](https://github.com/pierreguillot/Camomile)

## Pure data interface

Pure data has one main window with a menu bar at the top and blank space below. 
This empty space will act as a **console**.
All text based output will be shown here.
The program, or **patch** can be seen on the right.
In this window we can create **boxes** and connections between boxes to create a running program.

![interface overview](./resources/024l.jpg)

### TODO:

* create an empty patch and save it
* close puredata and reopen our saved empty patch

---

\newpage



## Example 1: Hello World

The two main types of boxes we need to create a hello world patch are:

* **object** boxes
* **message** boxes

In order to make our hello world function we need to connect the **outlet** of one to the **inlet** of the other.
You **always** have to create a connection for the **outlet** to the **inlet**.
The other way around does **not** work.

![hello world](./resources/xgr1.jpg)

Puredata has help built in to explain how all the objects function.
You can access this help by right clicking on a box and choosing **help**.
These help windows are also **patches** themselves which you can operate and copy paste from.

![help view](./resources/59e9.jpg)

Puredata has two **modes**:

* edit mode (to create your patch)
* normal mode (to click boxes)

I personally am almost always in **edit mode** because you can control the sliders, message boxes, etc by using **CTRL-click**.
This is a lot faster than switching back and forth between modes.

### TODO:

* create a hello world patch
* change the hello world text to something different
* create a second chain that prints out something different

### Challenge:

* try to differentiate between two different print chains by naming the print outputs

### Resources:

* **patches**
	* [hello world patch](./resources/00_hello_world.pd)

---

\newpage



## Example 2: A simple calculator

Arithmetic can be done with **object** boxes.
Besides object and message boxes we also have **number** boxes.
You can click and drag these boxes to change the number inside.

![arithmetic operations](./resources/zcwj.jpg)

![two calculators](./resources/p5h3.jpg)

### TODO:

* create this patch yourself and try it out
* why doesn't it always work?
* what are creation arguments?

### Resources:

* **patches**
	* [calculator patch](./resources/01_calculators.pd)
* **online information**
	* [hot and cold inlets](https://puredata.info/docs/manuals/pd/x2.htm#s3.3)

---

\newpage



## Example 3: Bangs and triggers

To deal with the **hot and cold** design of puredata we can use two very useful objects:

* bangs
* triggers

Bangs are an essential feature of puredata.
They serve as triggers and can be very useful for visual feedback.
Triggers take something (numbers, bangs, lists, messages, ...) as input and output from **right to left**.
By including triggers in our patch we can be certain of the **order of operations**.

![triggers and bangs](./resources/y0ps.jpg)

### TODO:

* create this patch yourself and try it out

### Resources:

* **patches**
	* [triggers and bangs patch](./resources/02_bangs_and_triggers.pd)

---

\newpage



## Example 4: Timers and toggles

As a real time environment puredata uses the **metro** object to create timed events.
To turn the metro on or off we need to connect a **toggle** to it's input.

![timers and toggles](./resources/zudu.jpg)

The metro object takes milliseconds as an argument *or* on the **cold** inlet.
Below is a small example conversion grid of BPM notes to msec.

![BPM to msec](./resources/bpm_msec.jpg)

### TODO:

* create this patch yourself and try it out
* can you make the output count up with each bang?
* can you make a conversion that takes BPM as input and outputs 8th notes msec values?

### Resources:

* **patches**
	* [timers and toggles patch](./resources/03_timers_and_toggles.pd)

---

\newpage



# Audio

## Setting up your audio device

From the **media** menu select the audio settings.
Your settings will be different depending on your platform and soundcard.
If you hear a lot of clicking then you should raise the block size.
A good middle ground is 1024.
Don't forget to turn on the **DSP** in the console window.
Puredata can output very loud sounds so don't turn the volume up too much.

<p style="text-align:center;color:red;">!!!**PLEASE** don't turn your headphones up too much!!!</p>

![audio device](./resources/uawy.jpg)

### Resources:

* **online information**
	* [configuring puredata](http://write.flossmanuals.net/pure-data/configuring/)

---

\newpage



## Example 5: A simple synth

Up until now we only used data objects and connections.
To create sound and connect audio inputs and output we use objects that have a tilde **~**.
New control objects, **sliders**, are seen here.
You can configure these sliders by right clicking them.

It's very important to modify the sliders in order to use them with audio signals.
All audio signals flow between -1 and +1 as floating point numbers.
In order to have working volume slider we multiply the audio signal with a value between 0 and 1 where 0.5 is half as loud.

![a simple synth](./resources/tz52.jpg)

### TODO:

* create this patch yourself and try it out

### Resources:

* **patches**
	* [a simple synth patch](./resources/04_a_simple_synth.pd)
* **online information**
	* [waveshaping](http://www.pd-tutorial.com/english/ch03s05.html)

---

\newpage



## Example 6: Some midi and music theory plus a musical synth

![midi notes to frequency](./resources/notes.gif)

The major scale is made up of the following pattern:

* a half note is **1** step
* a whole note is **2** steps
* an octave is **12** steps
* the **5th** always sounds good with the root note! (**7** steps)

| degree | name | step size | relative midi value |
|---|---|---|---|
| 1st | tonic | root | 0 |
| 2nd | supertonic | whole | 2 |
| 3rd | mediant | whole | 4 |
| 4th | subdominant | half | 5 |
| 5th | dominant | whole | 7 |
| 6th | submediant | whole | 9 |
| 7th | leading note | whole | 11 |
| 8th | tonic | half | 12 |

Below you can see the musical synth that plays the major scale starting at C4.

![a musical synth](./resources/u9us.jpg)

### Resources:

* **patches**
	* [a musical synth](./resources/05_a_musical_synth.pd)
* **online information**
	* [what is midi?](https://en.wikipedia.org/wiki/MIDI)
	* [musical scales](https://en.wikipedia.org/wiki/Major_scale)

---

\newpage



## Example 7: Controlling the musical synth

We can control the synth with the **key** object.
The output of this object needs to be routed to the corresponding notes we want with the **route** object.

![a musical synth with keyboard control](./resources/z847.jpg)

A more common way to control music programs is by using MIDI devices.
You can setup midi from the **media** menu and selecting **midi settings**.
Puredata provides a simple patch to test and see the midi messages coming into the computer.
It can be shown by clicking on the **media** menu and selecting **test audio and midi**.

![midi setup and settings](./resources/fnid.jpg)

### Resources:

* **patches**
	* [a musical synth](./resources/05_controlling_the_musical_synth.pd)
* **online information**
	* [controlling the synth](http://write.flossmanuals.net/pure-data/controlling-the-synth/)


---

\newpage



## Example 8: Controlling with a gamepad

The **hid** object gives us access to gamepads and joysticks.
Depending on the OS the object needs to be installed via the help menu **find
externals** option.

![gamepad synth](./resources/zz2h.jpg)

### TODO:

* create this patch yourself and try it out
* try to add more controls to the synth

### Resources:

* **patches**
	* [gamepad synth patch](./resources/07_gamepad_with_the_musical_synth.pd)
	* [gamepad synth with filter patch](./resources/08_gamepad_and_filter.pd)

---

\newpage



## Example 9: A drum machine

We can load and play soundfiles with the **readsf~** object.
To build a simple sequencer we combine a **metro** with a counting loop,
**mod** and **select**.

Below are the patches to both a simple and more complicated drum machine.

![drum machines](./resources/vy4d.jpg)

### TODO:

* create this patch yourself and try it out
* how can you make this drum machine polyphonic?

### Resources:

* **patches**
	* [simple drum machine](./resources/08_gamepad_and_filter.pd)
	* [advanced drum machine](./resources/09_advanced_drum_machine.pd)
* **samples**
	* [drum samples zip file](./resources/808_drum_kit.zip)

---

\newpage



## Example 10: Putting it *all* together

The advanced drum machine sequencer can be used to create a patch that plays
random *music* coupled with the simple synths from before.
A new object is included in this patch called **vline~**.
It is used to create volume or filter curves.

![putting it all together](./resources/8bpn.jpg)

### TODO:

* try to make something yourself!

### Resources:

* **patches**
	* [music by numbers](./resources/10_putting_it_all_together.pd)
* **online information**
	* [envelopes](http://write.flossmanuals.net/pure-data/envelope-generator/)

---

\newpage



## A short intermezzo

While it is very important and satisfying to make your own puredata patches from scratch, there are a lot of extra libraries that can provide you with essential building blocks.
Most of these can be installed with the build in package manager in puredata vanilla.
You can access it by going to the menu **help** and choosing **find externals**.
Some very useful ones are:

* mrpeach (for everything network and OSC related)
* list-abs (list abstractions that make manipulating and searching lists very easy)
* cyclone (a bunch of useful little gems)
* comport (for interfacing with arduino and other serial devices)
* hid (useful for gamepads and joysticks)

![installing externals](./resources/dniz.jpg)

You can see all the externals that you have installed by going to the **help** menu and choosing the **browser**.
Most libraries come with extensive help files to get you going in no time.


![externals browser](./resources/f33w.jpg)

Now, a *very* powerful collection of synthesizer building blocks is called **automatonism**.
This is more than a library of abstractions, it makes puredata an easy to use modular synth.
While I absolutely love this software, a basic understanding of puredata vanilla will go a long way.
You can download automatonism from the link below.

![automatonism](./resources/automatonism.png)

### Resources:

* **online information**
	* [automatonism](https://www.automatonism.com/the-software)

---

\newpage



# Video

## Creating a GEM window and drawing a pretty cube

GEM should come installed with puredata vanilla.
If not, you can find it via the find externals menu.
The best way to get familiar with GEM is to play around with the example files.
You can find them in the **browser**.

The most basic way to get GEM going is to:

* **declare -lib Gem** enables the library of use
* the **gemwin** object controls a window
* the **create, 1** message will display the empty window

![an empty GEM window](./resources/1ufh.jpg)

Now to display a simple shape in this window we need to add a **gemhead** object.
This will compute everything attached to it at every frame.

![a GEM cube drawn via gemhead](./resources/9gek.jpg)

This cube can be controlled with **messages** to the cube itself *and* by putting **objects** between the gemhead and itself.
The same cube but with rotation, color control (including transparency) and XYZ control can be seen below.
A light source is also added to introduce shade on the cube.

![3d cube with light and controls](./resources/1lv7.jpg)

To project something onto the cube we add a **pix_film** object.
This object needs to flow into a **pix_texture** object in order to be drawn.
The object loads .mp4 files and can play them back, in loop if desired.

![a dancing cat!](./resources/hla3.jpg)

### Resources:

* **patches**
	* [the assets](./resources/assets.zip)
	* [the dancing cat](./resources/11_gem_intro.pd)
* **online information**
	* [GEM intro](http://write.flossmanuals.net/pure-data/basics/)
	* [Ye olde GEM manual](https://puredata.info/downloads/gem/documentation/manual/manual)

## Particles

Besides displaying pictures and videos, GEM can animate particles.
The way to draw them is very similar to the dancing cat.
For each element we want to show and control separately we need different **gemhead**.
Download the test files below and have a play with it, it will explain itself a lot better like that.
Don't forget to download the assets as well, otherwise the video won't load.
You can find the link below (it's the same file as the previous example).

![a dancing cat with particles](./resources/rwch.jpg)

### Resources:

* **patches**
	* [the assets](./resources/assets.zip)
	* [a dancing cat with particles](./resources/11_gem_cat_with_particles.pd)
* **online information**
	* [GEM particles](https://puredata.info/downloads/gem/documentation/tutorial/particles)

# Learning more about Pure data

That's it for me but if you want to learn more about puredata you should have a look at the links below.
It's a list of tutorials and interesting work created with puredata.

* **documentation and tutorials**
	* [FLOSS manual](http://write.flossmanuals.net/pure-data/introduction2/)
	* [Rafael Hernadez youtube channel](https://www.youtube.com/playlist?list=PL12DC9A161D8DC5DC)
	* [puredata tutorials list](https://puredata.info/docs/tutorials/)
	* [Programming Electronic Music in Pd](http://www.pd-tutorial.com/english/index.html)
* **videos of sound made with pure data and some GEM stuff**
	* [puredata drone](https://www.youtube.com/watch?v=V1E52x15RYU)
	* [puredata sequencer 2015](https://www.youtube.com/watch?v=9Nz0bxwoqQE)
	* [72-EDO algorithmic IDM in Pure Data](https://www.youtube.com/watch?v=d4t5B6nPQGg)
	* [New Euclidean Rhythms Module in Pure Data Demo](https://www.youtube.com/watch?v=szFtVT5j69A)
	* [Pure Data - Particle-based Music Visualisation](https://www.youtube.com/watch?v=U1k44Tr0pu4)
	* [webe.omnibox / PureData Visual with GEM (Test_02)](https://www.youtube.com/watch?v=zs1lsw1q_gU)
